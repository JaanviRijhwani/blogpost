<?php

namespace App\Http\Middleware;

use App\Category;
use Closure;

class VerifyCategoriesCount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //If we use Category::all()->count() then it will fetch all the records and then it will count!
        //But if we use count() it will fetch like select count(*) from tablename!
        if(Category::count() === 0) {
            session()->flash('error', 'Minimum one category should exist to create a post');
            return redirect(route('categories.create'));
        }
        return $next($request);
    }//IMP NOTE: next is to register the middleware to the kernel!
}
