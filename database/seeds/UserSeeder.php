<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::where('email', 'jaanvirijhwani10@gmail.com')->get()->first();
        if(!$user){
            \App\User::create([
                'name'=>'Jaanvi Rijhwani',
                'email' => 'jaanvirijhwani10@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
                'role' => 'admin'
            ]);
        } else {
            $user->update(['role' => 'admin']);
        }

        \App\User::create([
            'name'=>'Shewta Parikh',
            'email' => 'shewta@gmail.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234')
        ]);

        \App\User::create([
            'name'=>'Yukta Peswani',
            'email' => 'yuktapeswani90@gmail.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
        ]);

    }
}
