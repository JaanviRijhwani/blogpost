@extends('layouts.app')

@section('page-level-scripts')

<!-- JQuery Core
    =====================================-->
<script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/core/bootstrap-3.3.7.min.js')}}"></script>

<!-- Magnific Popup
    =====================================-->
<script src="{{asset('assets/js/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/js/magnific-popup/magnific-popup-zoom-gallery.js')}}"></script>

<!-- JQuery Main
    =====================================-->
<script src="{{asset('assets/js/main/jquery.appear.js')}}"></script>
<script src="{{asset('assets/js/main/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('assets/js/main/parallax.min.js')}}"></script>
<script src="{{asset('assets/js/main/jquery.sticky.js')}}"></script>
<script src="{{asset('assets/js/main/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('assets/js/main/main.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
    console.log('Hello World!');
    
    $("#modalOpener").on("click", "#toggleModal", function () {
        $('#modalOpener').modal('show');
    });

    document.getElementById('title').onkeyup = document.getElementById('title').onkeypress = function () {
        document.getElementById('head').innerHTML = this.value;
    }
 
    document.getElementById('body-content').onkeyup = document.getElementById('body-content').onkeypress = function () {
        document.getElementById('body').innerHTML = this.value;
    }

    var tags = document.getElementById('tags');
    

    var tagSelected = [];
    document.getElementById('tags').onchange = function(){
        tagSelected = [];
        for(i=0; i<tags.length; i++)
        {
            
            if(tags.options[i].selected)
            {
                tagSelected.push(tags.options[i].text);

            }
        }
        console.log(tagSelected);
    document.getElementById('tag').innerHTML = tagSelected;
    }
    
    document.getElementById('imageAdd').onchange = function(){
        console.log(URL.createObjectURL(document.getElementById("imageAdd").files[0]));
        document.getElementById("blogImage").src = URL.createObjectURL(document.getElementById("imageAdd").files[0]);
    }   

</script>

<script>
    flatpickr("#published_at", {
        enableTime: true
    });
    $(document).ready(function () {
        $('.select2').select2();
    });

</script>
@endsection

@section('page-level-styles')
<!-- Favicons -->
<link rel="shortcut icon" href="{{asset('assets/img/favicon.png')}}">
<link rel="apple-touch-icon" href="{{asset('assets/img/apple-touch-icon.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/img/apple-touch-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/img/apple-touch-icon-114x114.png')}}">

<!-- Load Core CSS
    =====================================-->
<link rel="stylesheet" href="{{asset('assets/css/core/animate.min.css')}}">

<!-- Load Main CSS
    =====================================-->
<link rel="stylesheet" href="{{asset('assets/css/main/main.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/main/setting.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/main/hover.css')}}">


<link rel="stylesheet" href="{{asset('assets/css/color/pasific.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/icon/font-awesome.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/icon/et-line-font.css')}}">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<div class="card">
    <div class="card-header">Add Post</div>

    <div class="card-body">
        <form action="{{ route('posts.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title"
                    class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" value="{{ old('title')}}">

                @if ($errors->has('title'))
                <p class="text-danger font-weight-light">{{$errors->first('title')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="excerpt">Excerpt</label>
                <input type="text" name="excerpt" id="excerpt"
                    class="form-control @error('excerpt') is-invalid @enderror" value="{{ old('excerpt')}}">
                @error('excerpt')
                <p class="text-danger font-weight-light">{{$errors->first('excerpt')}}</p>
                @enderror
            </div>


            <div class="form-group">
                <label for="content">Content</label>
                <input type="hidden" name="content" id="content">
                <trix-editor input="content" id="body-content"></trix-editor>
                @error('content')
                <p class="text-danger font-weight-light">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label for="category_id"></label>
                <select name="category_id" id="category_id" class="form-control select2">
                    <option value="-1">Select Category</option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('category_id'))
                <p class="text-danger font-weight-light">{{$errors->first('category_id')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="tags">Tags</label>
                <select name="tags[]" id="tags" class="form-control select2" multiple>

                    @foreach($tags as $tag)
                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('tags'))
                <p class="text-danger font-weight-light">{{$errors->first('tags')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="published_at">Published At</label>
                <input type="date" name="published_at" id="published_at"
                    class="form-control {{ $errors->has('published_at') ? 'is-invalid' : '' }}"
                    value="{{ old('published_at')}}">

                @if ($errors->has('published_at'))
                <p class="text-danger font-weight-light">{{$errors->first('published_at')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="">Image</label>
                
                <input type="file" value="{{ old('image')}}" name="image" id="imageAdd"
                    class="form-control @error('image') is-invalid @enderror">
                @error('image')
                <p class="text-danger font-weight-light">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success">
            </div>
        </form>
    </div>
</div>

@endsection


@section('preview')
<div class="modal fade" id="toggleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-header">
                <h5>Preview!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row">
                <div class="container">
                    <div class="card">
                        <div class="card-body" id="modal-body">
                            <div class="blog-three-mini" id="data">
                                <h2 class="color-dark" id="head"></h2>
                                <div class="blog-three-attrib">
                                    <div><i class="fa fa-calendar"></i>Live Preview!</div> |
                                    <div><i class="fa fa-pencil"></i><a href="#"></a>{{auth()->user()->name}}</div> |

                                    <div>
                                        Share: <a href="#"><i class="fa fa-facebook-official"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                        <a href="#"><i class="fa fa-pinterest"></i></a>
                                    </div>
                                </div>
                                
                                    <img src="{{asset('storage/posts/default-image.jpg')}}" alt="Blog Image" class="img-responsive" id="blogImage" width="455px" height="300px">
                                
                                <div class="lead mt25" id="body">

                                </div>

                                <div class="blog-post-read-tag mt50">
                                    <i class="fa fa-tags"></i> Tags: <span id="tag" class="font-weight-light"></span>
                                </div>

                            </div>
                            <div class="blog-post-author mb50 pt30 bt-solid-1">
                                <img src="{{\Thomaswelton\LaravelGravatar\Facades\Gravatar::src(auth()->user()->email) }}"
                                    class="img-circle" height="30px" alt="image">
                                <span class="blog-post-author-name">{{auth()->user()->name}}</span> <a
                                    href="https://twitter.com/booisme"><i class="fa fa-twitter"></i></a>
                                <p class="text-weight-light">
                                    @if(auth()->user()->about == null)
                                        N.A.
                                    
                                    @else 
                                            {{auth()->user()->about}}
                                
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
